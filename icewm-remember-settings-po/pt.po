# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# José Vieira <jvieira33@sapo.pt>, 2023
# Paulo C., 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:46+0200\n"
"PO-Revision-Date: 2023-02-28 15:49+0000\n"
"Last-Translator: Paulo C., 2023\n"
"Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: icewm-remember-settings:108
msgid "HELP"
msgstr "AJUDA"

#: icewm-remember-settings:109
msgid ""
"Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a "
"window using the IceWM-remember-settings app."
msgstr ""
"Grava o <b>tamanho e posição</b>, <b>área de trabalho</b> e <b>camada</b> de"
" uma janela, usando a app IceWM-remember-settings."

#: icewm-remember-settings:111
msgid ""
"Next time you launch the program, it will remember the window properties "
"last saved."
msgstr ""
"Da próxima vez que iniciar a aplicação, serão mantidas as propriedades da "
"janela guardadas."

#: icewm-remember-settings:113
msgid "You can also delete this information unticking all options."
msgstr ""
"Pode também eliminar esta informação ao remover os vistos de todas as "
"opções. "

#: icewm-remember-settings:114
msgid ""
"Use the <b>Select other</b> option to select a different window/program to "
"configure."
msgstr ""
"Use a opção <b>Escolher Outra </b> para escolher uma janela/app diferente "
"para configurar."

#: icewm-remember-settings:126 icewm-remember-settings:219
msgid "Add/Remove IceWM Window Defaults"
msgstr "Adicionar/Remover padrões de Janelas do IceWM"

#: icewm-remember-settings:127
msgid "<b>Select a program. Store it's window properties.</b>"
msgstr "<b> Escolha um programa. Preservar as propriedades da janela. </b>"

#: icewm-remember-settings:128
msgid "What window configuration you want antiX to remember/forget?"
msgstr "Que configuração de janela pretende que o antiX memorize/esqueça?"

#: icewm-remember-settings:220
#, sh-format
msgid ""
"Entries shown below are for the <b>$appclass</b> ($appname) window.\\n\\nAll"
" options marked will be saved, all unmarked will be deleted.\\n\\n Note: "
"Workspace number shown is the window's current workspace. \\n \tDon't worry "
"that it appears too low.\\n\\n"
msgstr ""
"As entradas exibidas abaixo são para a janela <b> $appclass</b> ($appname) "
".\\n\\nTodas as opções marcadas serão gravadas, todas as que estão em branco"
" serão eliminadas.\\n\\n Nota: O número da área de trabalho é exibido na "
"área de trabalho atual da janela. \\n\t Não se preocupe se parecer ser muito"
" diminuto.\\n\\n"

#: icewm-remember-settings:225
msgid "Select"
msgstr "Seleccionar"

#: icewm-remember-settings:225
msgid "Type"
msgstr "Tipo"

#: icewm-remember-settings:225
msgid "Value"
msgstr "Valor"

#: icewm-remember-settings:226
msgid "Geometry"
msgstr "Geometria"

#: icewm-remember-settings:227
msgid "Layer"
msgstr "Camada"

#: icewm-remember-settings:227
msgid "workspace"
msgstr "Área de trabalho"

#: icewm-remember-settings:229
msgid "Select other"
msgstr "Escolher outra"
