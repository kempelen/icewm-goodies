��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     3     Q  /   j  �   �          #     2  e   @  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Kimmo Kujansuu <mrkujansuu@gmail.com>, 2023
Language-Team: Finnish (https://app.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Lisää (manuaalinen) komento Lisää ohjelma listasta Anna käynnistystiedostoon lisättävä komento Anna komento, jonka suoritetaan antiX:n käynnistyksen yhteydessä. Huomaa: merkki/& liitetään automaattisesti komennon loppuun Poista Poista ohjelma Käynnistys ( Rivi lisättiin tiedostoon $startupfile. Tämä käynnistyy automaattisesti, kun käynnistät antiX:n \n $add_remove_text $startupfile): antiX-startup GUI 