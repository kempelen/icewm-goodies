��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  b  �     
            K   )  Z   u     �      �  	   �  7     K   :  �   �     5  #   P  "   t  #   �  "   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Anwenden Zurück Namen ändern Auf Anwenden klicken, wenn die Namen der Arbeitsbereiche festgelegt wurden. Klicken sie auf Fertig, wenn die gewünschte Anzahl von Arbeitsbereichen festgelegt wurde. Fertig Den gewünschten Namen eingeben: Verlassen Die gewünschte Anzahl von Arbeitsoberflächen eingeben Um die Namen der Arbeitsbereiche zu ändern, klicken sie auf Namen ändern. Verwenden sie das Mausrad oder klicken sie auf die Pfeile. Sie können eine Zahl auch über die Tastatur eingeben, gefolgt von der Eingabetaste. Die Änderung erfolgt sofort. Nummer des Arbeitsbereichs antiX-Arbeitsbereichsanzahl-Manager antiX-Arbeitsbereichsnamen-Manager antiX-Arbeitsbereichsanzahl-Manager antiX-Arbeitsbereichsnamen-Manager 