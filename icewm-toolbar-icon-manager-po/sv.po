# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# anticapitalista <anticapitalista@riseup.net>, 2020
# Henry Oquist <henryoquist@nomalm.se>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:47+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023\n"
"Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-toolbar-icon-manager.sh:13
msgid "You are running an IceWM desktop"
msgstr "Du använder ett IceWM-skrivbord"

#: icewm-toolbar-icon-manager.sh:15 icewm-toolbar-icon-manager.sh:121
msgid "Warning"
msgstr "Varning"

#: icewm-toolbar-icon-manager.sh:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "Detta skript är endast avsett att köras i ett IceWM-skrivbord"

#: icewm-toolbar-icon-manager.sh:77 icewm-toolbar-icon-manager.sh:86
#: icewm-toolbar-icon-manager.sh:102 icewm-toolbar-icon-manager.sh:135
#: icewm-toolbar-icon-manager.sh:155 icewm-toolbar-icon-manager.sh:335
msgid "Toolbar Icon Manager"
msgstr "Ikonhanterare för Verktygsfält"

#: icewm-toolbar-icon-manager.sh:77
msgid "Help::TXT"
msgstr "Help::TXT"

#: icewm-toolbar-icon-manager.sh:77
msgid ""
"What is this?\\nThis utility adds and removes application icons to IceWm's toolbar.\\nThe toolbar application icons are created from an application's .desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\\nNote: some of antiX's applications are found in the sub-folder 'antiX'.\\n\n"
"TIM buttons:\\n 'ADD ICON' - select, from the list, the .desktop file of the application you want to add to your toolbar and it instantly shows up on the toolbar.\\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\\n'UNDO LAST STEP' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar\\n'MOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to select it and then move it to the left or to the right\\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearrange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\\n Warnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file!"
msgstr ""
" Vad är detta?\\nDetta verktyg lägger till och tar bort ikoner från IceWm's verktygsfält.\\nVerktygsfältets program-ikoner skapas från programmets .desktop fil.\\nVad är .desktop filer?\\nVanligen skapas en .desktop fil under ett programs installationsprocess för att ge systemet lätt tillgång till relevant information, som programmets hela namn, kommandon som ska utföras, ikonen som kommer att användas, var den kommer att placeras i operativsystemets meny, etc.\\nEn .desktop fils namn refererar vanligen till appens namn, vilket gör det mycket lätt att hitta den sökta .desktop filen (ex: Firefox ESR's .desktop fil är 'firefox-esr.desktop').\\nNär man lägger till en ny ikon i verktygsfältet, kan användaren klicka på fätet som visas i huvudfönstret och en lista över alla installerade programs .desktop filer kommer att visas.\\nDet är i själva verket en lista över (nästan) alla installerade program, som kan läggas till i verktygsfältet.\\nAnmärkning: några av antiX's program kan hittas i undermappen 'antiX'.\\n\n"
"TIM knappar:\\n'LÄGG TILL IKON' - välj från listan, .desktop filen för programmet du vill lägga till i verktygsfältet så visas den genast i verktygsfältet.\\nOm, av någon orsak, TIM inte kan hitta rätt ikon för ditt program, kommer den ändå att skapa en verktygsfälts-ikon med standardiserad 'kugghjulsbild' så du fortfarande kan klicka för att öppna programmet.\\nDu kan klicka på 'Avancerat' knappen för att manuellt redigera den relevanta posten och byta programikon.\\nÅNGRA SENASTE STEG' - varje gång en ikon läggs till eller tas bort från verktygsfältet, skapar TIM en backup-fil. Om du klickar på denna knapp, kommer verktygsfältet omedelbart att återställas från backup-filen, utan någon bekräftelse.\\n'TA BORT ikon' - denna visar en lista över alla program som har ikoner i verktygsfältet Dubbel-vänsterklicka på ett program för att ta bort dess ikon från verktygsfältet.\\n'FLYTTA IKON' - visar en lista över alla applikationer som har ikoner i verktygsfältet. Dubbel-vänsterklicka på en applikation för att välja den och flytta den sedan till vänster eller höger\\n'AVANCERAT' - tillåter redigering av den textkonfigurationsfil som innehåller ditt skrivbords alla verktygsfältsikoners konfigurationer. Att manuellt redigera denna fil tillåter användaren att arrangera om ikonernas ordning och ta bort eller lägga till en ikon. En kort förklaring av textkonfigurationsfilen visas innan filen öppnas för redigering.\\n Varningar: redigera endast en konfigurationsfil manuellt om du vet vad du gör! Gör alltid en backup-kopia innan du redigerar en konfigurationsfil! "

#: icewm-toolbar-icon-manager.sh:86
msgid "Warning::TXT"
msgstr "Varning::TXT"

#: icewm-toolbar-icon-manager.sh:86
msgid ""
"If you click 'Yes', the toolbar configuration file will be opened for manual editing.\\n\n"
"How-to:\\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each toolbar icon entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nSave any changes and then restart IceWM.\\nYou can undo the last change from TIMs UNDO LAST STEP button."
msgstr ""
"Om du klickar'Ja' , kommer verktygsfältets konfigurationsfil att vara öppen för manuell redigering.\\n\n"
"How-to:\\nVarje verktygsfältsikon identifieras av en rad som börjar med 'prog' följt av programnamnet, ikon och programmets körbara fil.\\nFlytta, redigera eller ta bort hela den rad som refererar till en verktygsfältsikon-post.\\nAnmärkning: Rader som börjar med # är enbart kommentarer och kommer att ignoreras.\\nTomma rader kan förekomma.\\nSpara ändringar och starta sedan om IceWM.\\nDu kan ångra den senaste ändringen med TIM's ÅNGRA SISTA STEGET knapp."

#: icewm-toolbar-icon-manager.sh:87
msgid "Ok"
msgstr "OK"

#: icewm-toolbar-icon-manager.sh:102
msgid "Double click any Application to remove its icon:"
msgstr "Dubbelklicka på  en Applikation för att ta bort dess ikon:"

#: icewm-toolbar-icon-manager.sh:102
msgid "Remove"
msgstr "Ta bort"

#: icewm-toolbar-icon-manager.sh:113
msgid "file has something"
msgstr "filen har något"

#: icewm-toolbar-icon-manager.sh:119
msgid "file is empty"
msgstr "filen är tom"

#: icewm-toolbar-icon-manager.sh:121
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Inga ändringar gjordes!\\nTIPS: du kan alltid försöka med Avancerat-knappen."

#: icewm-toolbar-icon-manager.sh:135
msgid "Double click any Application to move its icon:"
msgstr "Dubbelklicka på en Applikation för att flytta dess ikon:"

#: icewm-toolbar-icon-manager.sh:135
msgid "Move"
msgstr "Flytta"

#: icewm-toolbar-icon-manager.sh:145
msgid "nothing was selected"
msgstr "inget valdes"

#: icewm-toolbar-icon-manager.sh:155
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Välj vad som ska göras med $EXEC ikon"

#: icewm-toolbar-icon-manager.sh:157
msgid "Move left"
msgstr "Flytta till vänster"

#: icewm-toolbar-icon-manager.sh:158
msgid "Move right"
msgstr "Flytta till höger"

#: icewm-toolbar-icon-manager.sh:223
msgid "Add selected app's icon"
msgstr "Lägg till vald apps ikon"

#: icewm-toolbar-icon-manager.sh:223
msgid "Choose application to add to the Toolbar"
msgstr "Välj applikation att lägga till i Verktygsfältet"

#: icewm-toolbar-icon-manager.sh:339
msgid "HELP!help:FBTN"
msgstr "HJÄLP!help:FBTN"

#: icewm-toolbar-icon-manager.sh:340
msgid "ADVANCED!help-hint:FBTN"
msgstr "AVANCERAT!help-hint:FBTN"

#: icewm-toolbar-icon-manager.sh:341
msgid "ADD ICON!add:FBTN"
msgstr "LÄGG TILL IKON!add:FBTN"

#: icewm-toolbar-icon-manager.sh:342
msgid "REMOVE ICON!remove:FBTN"
msgstr "TA BORT IKON!remove:FBTN"

#: icewm-toolbar-icon-manager.sh:343
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "FLYTTA IKON!gtk-go-back-rtl:FBTN"

#: icewm-toolbar-icon-manager.sh:344
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "ÅNGRA SISTA STEGET!undo:FBTN"

#: icewm-toolbar-icon-manager.sh:345
msgid ""
"Please select any option from the buttons below to manage Toolbar icons"
msgstr ""
"Var vänlig välj ett alternativ bland knapparna nedan för att hantera "
"Verktygsfältets ikoner"
