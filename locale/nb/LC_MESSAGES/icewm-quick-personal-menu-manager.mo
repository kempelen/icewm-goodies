��          �      |      �     �            !   3  .   U  0   �     �  	   �     �  D   �           9     Q  7   X     �     �     �      �     �     �        �       �     �     �  *   �  8     8   P     �  
   �     �  A   �  &   �          *  <   0     m     �     �     �     �     �     �     
                                                                        	                    ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Move No changes were made!\nTIP: you can always try the Advanced buttton. Personal Menu Ultra Fast Manager REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 LEGG TIL IKON!add:FBTN AVANSERT!help-hint:FBTN Legg til valgt programs ikon Velg hva som skal gjøres med $EXEC-ikonet Dobbeltklikk et vilkårlig program for å flytte ikonet: Dobbeltklikk et vilkårlig program for å fjerne ikonet: HJELP!help:FBTN Hjelp::TXT Flytt Ingen endringer ble utført.\nHint: Forsøk knappen «Avansert». Ultrarask behandling av personlig meny FJERN IKON!remove:FBTN Fjern Dette skriptet bør kun kjøres i et IceWM-skrivebordsmiljø ANGRE SISTE STEG!undo:FBTN Advarsel Advarsel::TXT Du kjører et IceWM-skrivebord filen er ikke tom filen er tom ingenting ble valgt 