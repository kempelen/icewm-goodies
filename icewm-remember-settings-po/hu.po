# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Feri, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:46+0200\n"
"PO-Revision-Date: 2023-02-28 15:49+0000\n"
"Last-Translator: Feri, 2023\n"
"Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-remember-settings:108
msgid "HELP"
msgstr "SÚGÓ"

#: icewm-remember-settings:109
msgid ""
"Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a "
"window using the IceWM-remember-settings app."
msgstr ""
"Mentse el az ablakok <b>méretét és helyét</b>, <b>munkaterületét</b> és "
"<b>sorrendjét</b> az IceWM beállítások megjegyzése alkalmazással."

#: icewm-remember-settings:111
msgid ""
"Next time you launch the program, it will remember the window properties "
"last saved."
msgstr ""
"A program a következő indításakor emlékezni fog az ablakok legutóbb "
"elmentett tulajdonságaira."

#: icewm-remember-settings:113
msgid "You can also delete this information unticking all options."
msgstr ""
"Törölheti is ezeket az információkat az opciók kijelölésének törlésével."

#: icewm-remember-settings:114
msgid ""
"Use the <b>Select other</b> option to select a different window/program to "
"configure."
msgstr ""
"Használja a <b>Másik választása</b> opciót egy másik ablak vagy program "
"beállításához."

#: icewm-remember-settings:126 icewm-remember-settings:219
msgid "Add/Remove IceWM Window Defaults"
msgstr "IceWM ablak alapértékek hozzáadása/eltávolítása"

#: icewm-remember-settings:127
msgid "<b>Select a program. Store it's window properties.</b>"
msgstr "<b>Válasszon egy programot. Tárolja az ablak jellemzőit.</b>"

#: icewm-remember-settings:128
msgid "What window configuration you want antiX to remember/forget?"
msgstr ""
"Melyik ablakbeállításokra szeretné, hogy az antiX emlékezzen vagy "
"elfelejtse?"

#: icewm-remember-settings:220
#, sh-format
msgid ""
"Entries shown below are for the <b>$appclass</b> ($appname) window.\\n\\nAll"
" options marked will be saved, all unmarked will be deleted.\\n\\n Note: "
"Workspace number shown is the window's current workspace. \\n \tDon't worry "
"that it appears too low.\\n\\n"
msgstr ""
"Az alább megjelenített értékek a <b>$appclass</b> ($appname) ablakra "
"vonatkoznak.\\n\\nMinden bejelölt tulajdonság mentésre kerül, a nem "
"bejelöltek pedig törlésre.\\n\\nMegjegyzés: A munkaterület megjelenített "
"száma az ablak jelenlegi munkaterülete.\\n\tNe aggódjon, ha túl alacsonynak "
"tűnik.\\n\\n"

#: icewm-remember-settings:225
msgid "Select"
msgstr "Választás"

#: icewm-remember-settings:225
msgid "Type"
msgstr "Típus"

#: icewm-remember-settings:225
msgid "Value"
msgstr "Érték"

#: icewm-remember-settings:226
msgid "Geometry"
msgstr "Geometria"

#: icewm-remember-settings:227
msgid "Layer"
msgstr "Sorrend"

#: icewm-remember-settings:227
msgid "workspace"
msgstr "munkaterület"

#: icewm-remember-settings:229
msgid "Select other"
msgstr "Másik választása"
