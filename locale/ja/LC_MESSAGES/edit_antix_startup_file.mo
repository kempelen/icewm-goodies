��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  y  �  -   '  -   U  M   �  �   �     n  !   u  	   �  l   �  "        1                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Green <usergreen@users.osdn.me>, 2023
Language-Team: Japanese (https://app.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 （マニュアルな）コマンドを追加 一覧からアプリケーションを追加 startup ファイルへ追加したいコマンドを入力してください antiX の起動時に実行したいコマンドを入力します。注意 - コマンドの末尾にはアンパサンド/& を自動的に付加します 削除 アプリケーションを削除 Startup ( この行を $startupfile へ追加しました。次回の antiX 起動時、自動的に起動します。 \n $add_remove_text $startupfile): antiX 自動起動 GUI 