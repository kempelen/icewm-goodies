��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �  
   ,     7     >  R   G  B   �     �     �     �  )     :   2  �   m       %     #   =      a     �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Verkställ Bakåt Byt Namn Klicka på Verkställ när de önskade namnen på arbetsområdena har skrivits in. Klicka på Klar när önskat antal arbetsområden har ställts in. Klar Skriv in de önskade namnen:  Avsluta Ställ in det önskade antalet skrivbord: För att byta namn på Arbetsområden klicka på Byt Namn. Använd mushjulet eller klicka på pilar. Du kan också skriva in ett antal med tangentbordet följt av Enter-tangenten. Ändringen sker omedelbart. Arbetsområde Nummer aWCS antiX arbetsområde räknebytare aWCS antiX arbetsområde namnbytare antiX arbetsområde räknebytare antiX arbetsområde namnbytare 