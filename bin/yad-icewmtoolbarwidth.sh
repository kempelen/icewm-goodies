#!/bin/sh
#GUI to manager IceWM Toolbar Width, by PPC
#Variables to store localizable strings:
window_title="IceWM Toolbar Width"
position_description="Position"

###first make sure that it exists in prefoverride, then make sure it's set to 0
file=$HOME/.icewm/prefoverride
text="TaskBarWidthPercentage="
#Check if string exists in  file, if it does not, add it and make sure it's set to 100 (its the default):
if cat $file | grep ^$text; then
   # echo found, making sure its not commented out
    sed -i "/$text/s/^#//g" $file
else
   # echo not found
    echo ${text}100 >> $file
fi

text="TaskBarJustify="
#Check if string exists in  file, if it does not, add it and make sure it's set to 100 (its the default):
if cat $file | grep ^$text; then
   # echo found, making sure its not commented out
    sed -i "/$text/s/^#//g" $file
else
   # echo not found
    echo ${text}"left" >> $file
fi

current_size=$(sed -n '/TaskBarWidthPercentage/p' $HOME/.icewm/prefoverride| cut -f2- -d=| head -n1 | cut -d " " -f1)
yad --button="OK" --width=350 --center --title="$window_title" --form --separator="," \
--field="% ::NUM" $current_size!25..100!1!0 \
--field="$position_description::CB" "center!left!right" \
"" "" ""| while read line; do
POSITION=`echo $line | awk -F',' '{print $2}'`
SIZE=`echo $line | awk -F',' '{print $1}'`
#Toggle TaskBarWidthPercentage:
sed -i "s/TaskBarWidthPercentage=.*/TaskBarWidthPercentage=${SIZE}/" $HOME/.icewm/prefoverride
sed -i "s/TaskBarJustify=.*/TaskBarJustify=${POSITION}/" $HOME/.icewm/prefoverride
#restart icewm, to see the change:
icewm -r
#Exit function:
###return
done

