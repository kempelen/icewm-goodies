��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  n  �               (  ;   5  ;   q     �     �     �  #   �  5   �  �   +     �  '   �  "   �  "        +                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Risto Pärkkä, 2023
Language-Team: Finnish (https://www.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Hyväksy Takaisin Vaihda nimet Klikkaa hyväksy kun halutut työtilojen nimet ovat valittu Klikkaa valmis kun haluttu työtilojen määrä on asetettu Valmis Valitse halutut nimet Poistu Aseta haluttu määrä työpöytiä Vaihtaaksesi työtilojen nimiä, klikkaa vaihda nimet Käytä hiiren rullaa tai klikkaa hiirellä. Voit myös laittaa numeron näppäimistöllä. Muutokset tapahtuvat välittömästi Työtilan numero aWCS antiX työtilan määrän vaihtaja aWCS antiX työtilan nimenvaihtaja antiX työtilan määrän vaihtaja antiX työtilan nimenvaihtaja 