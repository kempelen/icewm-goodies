��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     c     l     r  6   �  I   �          
     !  "   )  B   L  �   �       /   5  /   e  *   �  *   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Uveljavi Nazaj Spremeni imena Po vnosu imen delovnih površin, kliknite na Uveljavi. Po določitvi ustreznega števila delovnih površin, kliknite na Končaj. končaj Vnesite želeno imena: Zapusti Vnesite željeno število namizji: Za spreminjanje imen delovnih površin kliknite na Spremeni imena. Uproabite kolesce miške ali kiknite na puščice. Število lahko vnesete tudi s tipkovnico in tpiko enter. Sprememba se izvede nemudoma. Številka delovne površine aWCS antiX preklopnik števca delovnih površin aWCS antiX spreminjanje imena delovne površine antiX preklopnik števca delovnih površin antiX spreminjanje imena delovne površine 