��          �      l      �     �     �       !   #  .   E  0   t     �  	   �     �  D   �             7   '     _     x     �      �     �     �     �  �  �     t  &   �  +   �  #   �  @     C   C     �  
   �     �  F   �     �       D     !   Y     {  
   �  $   �     �     �     �     
                                                                         	                    ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Move No changes were made!\nTIP: you can always try the Advanced buttton. REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2023
Language-Team: Galician (Spain) (https://app.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 AGREGAR ICONA!add:FBTN CONFIGURACIÓN AVANZADA!help-hint:FBTN Engadir a icona da aplicación seleccionada Escoller que facer coa icona $ EXEC Faga dobre clic en calquera aplicación para mover a súa icona: Faga dobre clic en calquera aplicación para eliminar a súa icona: AXUDA!help:FBTN Axuda::TXT Mover Non se fixeron cmabios!\nConsello: intentar co botón 'Conf AVANZADA'. ELIMINAR ÍCONA!remove:FBTN Eliminar Este script está destinado para executarse só nun escritorio IceWM REVERTIR O ÚLTIMO PASO!undo:FBTN Aviso Aviso::TXT Está a executar un escritorio IceWM o ficheiro ten algo o ficheiro está vacío non se seleccionou nada 