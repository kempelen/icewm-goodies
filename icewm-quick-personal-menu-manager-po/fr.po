# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Franck Vazifranky <vazifranky@gmail.com>, 2023
# sombreros <sebastiendebierre@gmail.com>, 2023
# cyril cottet <cyrilusber2001@yahoo.fr>, 2023
# Wallon Wallon, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-19 16:07+0200\n"
"PO-Revision-Date: 2023-02-19 14:12+0000\n"
"Last-Translator: Wallon Wallon, 2023\n"
"Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: icewm-quick-personal-menu-manager:13
msgid "You are running an IceWM desktop"
msgstr "Vous utilisez un bureau IceWM"

#: icewm-quick-personal-menu-manager:15 icewm-quick-personal-menu-manager:119
msgid "Warning"
msgstr "Attention"

#: icewm-quick-personal-menu-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "Ce script est prévu pour fonctionner uniquement dans un bureau IceWM"

#: icewm-quick-personal-menu-manager:77 icewm-quick-personal-menu-manager:85
#: icewm-quick-personal-menu-manager:101 icewm-quick-personal-menu-manager:133
#: icewm-quick-personal-menu-manager:153 icewm-quick-personal-menu-manager:218
#: icewm-quick-personal-menu-manager:327
msgid "Personal Menu Ultra Fast Manager"
msgstr "Gestionnaire ultra-rapide du menu personnel"

#: icewm-quick-personal-menu-manager:77
msgid "Help::TXT"
msgstr "Aide::TXT"

#: icewm-quick-personal-menu-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons from IceWm's"
" 'personal' list.\\nApplication icons are created from an application's "
".desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\n.\\n Buttons:\\n 'ADD ICON' - select, from the list, application you "
"want to add to your 'personal' list and it instantly shows up in the menu or"
" submenu.\\nIf, for some reason, the correct icon for your application is "
"not found, a 'gears' icon will be used, so that you can still click it to "
"access the application.\\nYou can click the 'Advanced' button to manually "
"edit the relevant entry and change the application's icon.\\n'UNDO LAST "
"STEP' - every time an icon is added or removed from the toolbar, A backup "
"file is created. If you click this button, a restore is performed from that "
"backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list "
"of all applications that have icons on your 'personal' list. Double left "
"click any application to remove its icon from the list\\n'ADVANCED' - allows"
" for editing the text configuration file that stores all of the entries in "
"your  'personal' list. Manually editing this file allows users to rearrange "
"the order of the icons and delete or add any icon. A brief explanation about"
" the inner workings of the text configuration file is displayed before the "
"file is opened for edition.\\n Warnings: only manually edit a configuration "
"file if you are sure of what you are doing! Always make a back up copy "
"before editing a configuration file!"
msgstr ""
"De quoi s’agit-il ?\\nCet utilitaire ajoute et supprime les icônes "
"d’application de la liste « personnelle » d’IceWm.\\nLes icônes "
"d’application sont créées à partir du fichier .desktop d’une "
"application.\\nQue sont les fichiers .desktop ?\\nEn général, un fichier "
".desktop est créé au cours du processus d’installation d’une application "
"pour permettre au système d’accéder facilement aux informations pertinentes,"
" telles que le nom complet de l’application, les commandes à exécuter, "
"l’icône à utiliser, l’emplacement dans le menu du système d’exploitation, "
"etc.\\n.\\n Les boutons :\\n « AJOUTER UNE ICÔNE » - choisissez, dans la "
"liste, l’application que vous souhaitez ajouter à votre liste « personnelle "
"» et elle apparaît instantanément dans le menu ou le sous-menu.\\nSi, pour "
"une raison ou une autre, l’icône correspondant à votre application n’est pas"
" trouvée, une icône « engrenage » sera utilisée, de sorte que vous pourrez "
"toujours cliquer dessus pour accéder à l’application.\\nVous pouvez cliquer "
"sur le bouton « Avancé » pour modifier manuellement l’entrée correspondante "
"et changer l’icône de l’application.\\n« ANNULER LA DERNIÈRE ÉTAPE » - "
"chaque fois qu’une icône est ajoutée ou retirée de la barre d’outils, un "
"fichier de sauvegarde est créé. Si vous cliquez sur ce bouton, une "
"restauration est effectuée à partir de ce fichier de sauvegarde, sans aucune"
" confirmation.\\n« SUPPRIMER UNE ICÔNE » - cela affiche une liste de toutes "
"les applications qui ont des icônes dans votre liste « personnelle ». "
"Double-cliquez avec le bouton gauche de la souris sur une application pour "
"supprimer son icône de la liste.\\n« AVANCÉ » - permet de modifier le "
"fichier de configuration texte qui stocke toutes les entrées de votre liste "
"« personnelle ». L’édition manuelle de ce fichier permet aux utilisateurs de"
" réorganiser l’ordre des icônes et de supprimer ou d’ajouter toute icône. "
"Une brève explication sur le fonctionnement interne du fichier texte de "
"configuration est affichée avant l’ouverture du fichier pour l’édition.\\n "
"Avertissements : ne modifiez manuellement un fichier de configuration que si"
" vous êtes sûr de ce que vous faites ! Faites toujours une copie de "
"sauvegarde avant de modifier un fichier de configuration !"

#: icewm-quick-personal-menu-manager:85
msgid "Warning::TXT"
msgstr "Attention::TXT"

#: icewm-quick-personal-menu-manager:85
msgid ""
"If you click to continue, the 'personal' configuration file will be opened for manual edition.\\n\n"
"How-to:\\nEach icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nAny changes appear instantly on the menu.\\nYou can undo the last change from UNDO LAST STEP button."
msgstr ""
"Si vous cliquez sur continuer, le fichier de configuration « personnel » sera ouvert pour une édition manuelle.\\n\n"
"Mode d’emploi :\\nChaque icône est identifiée par une ligne commençant par « prog », suivi du nom de l’application, de l’icône et du fichier exécutable de l’application.\\n Déplacez, modifiez ou supprimez la ligne entière se rapportant à chaque entrée.\\nRemarque : Les lignes commençant par # ne sont que des commentaires et seront ignorées.\\nIl peut y avoir des lignes vides.\\nToute modification apparaît instantanément dans le menu.\\nVous pouvez annuler la dernière modification à l’aide du bouton « ANNULER LA DERNIÈRE ÉTAPE »."

#: icewm-quick-personal-menu-manager:101
msgid "Double click any Application to remove its icon:"
msgstr "Double-cliquez sur une application pour supprimer son icône :"

#: icewm-quick-personal-menu-manager:101
msgid "Remove"
msgstr "Supprimer"

#: icewm-quick-personal-menu-manager:112
msgid "file has something"
msgstr "Le fichier a quelque chose"

#: icewm-quick-personal-menu-manager:117
msgid "file is empty"
msgstr "Le fichier est vide"

#: icewm-quick-personal-menu-manager:119
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Aucun changement effectué !\\nASTUCE : vous pouvez toujours essayer le "
"bouton Avancé."

#: icewm-quick-personal-menu-manager:133
msgid "Double click any Application to move its icon:"
msgstr ""
"Double-cliquez sur n’importe quelle application pour déplacer son icône :"

#: icewm-quick-personal-menu-manager:133
msgid "Move"
msgstr "Déplacer"

#: icewm-quick-personal-menu-manager:143
msgid "nothing was selected"
msgstr "rien n’a été sélectionné"

#: icewm-quick-personal-menu-manager:153
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Choisissez ce que vous voulez faire avec l’icône $EXEC"

#: icewm-quick-personal-menu-manager:218
msgid "Add selected app's icon"
msgstr "Ajouter l’icône de l’application sélectionnée"

#: icewm-quick-personal-menu-manager:331
msgid "HELP!help:FBTN"
msgstr "AIDE!help:FBTN"

#: icewm-quick-personal-menu-manager:332
msgid "ADVANCED!help-hint:FBTN"
msgstr "AVANCÉ!help-hint:FBTN"

#: icewm-quick-personal-menu-manager:333
msgid "ADD ICON!add:FBTN"
msgstr "AJOUTER UNE ICÔNE!add:FBTN"

#: icewm-quick-personal-menu-manager:334
msgid "REMOVE ICON!remove:FBTN"
msgstr "SUPPRIMER UNE ICÔNE!remove:FBTN"

#: icewm-quick-personal-menu-manager:335
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "ANNULER LA DERNIÈRE ÉTAPE!undo:FBTN"
