��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  j  �       &   3  7   Z  �   �     '     6     R  g   ^  "   �  $   �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Kempelen, 2023
Language-Team: Hungarian (https://app.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Parancs kézi hozzáadása Alkalmazás hozzáadás egy listáról Írja be az indítási fájlhoz hozzáadandó parancsot Írja be a parancsot amelyet le szeretne futtatni az antiX indításakor. Megjegyzés: az & jel automatikusan hozzáfűzésre kerül a sor végéhez Eltávolítás Alkalmazás eltávolítása Indítás ( A sor bekerült a $startupfile fájlba. Az antiX következő indításakor automatikusan el fog indulni \n $add_remove_text $startupfile): antiX indítási fájl szerkesztése 