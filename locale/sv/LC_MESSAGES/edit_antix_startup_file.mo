��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �  "   2  !   U  3   w  �   �     4     <  	   P  `   Z  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Lägg till ett (manuellt) kommando Lägg till program från en lista Skriv in kommando att läggas till i startup-filen. Skriv in kommando du vill köra vid antiXs start. Anmärkning: ett ampersand/& kommer att automatiskt läggas till i slutet av kommandot Ta bort Ta bort ett program Startup ( Raden lades till i $startupfile. Den kommer att starta automatiskt nästa gång du startar antiX \n $add_remove_text $startupfile): antiX-startup GUI 