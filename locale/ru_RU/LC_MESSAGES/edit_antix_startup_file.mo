��          t      �                 (  -   D     r     �     �  	     ^     "   x     �    �  0   �  7   �  T     �   o     _  #   n     �  �   �  "   O  =   r                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Victor Red, 2024
Language-Team: Russian (Russia) (https://app.transifex.com/anticapitalista/teams/10162/ru_RU/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru_RU
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Добавить (вручную) команду Добавить приложение из списка Введите команду для добавления в файл запуска Введите команду, которую вы хотите запустить при запуске antiX. Примечание: амперсанд/& будет автоматически добавлен в конец команды. Удалить Удалить приложение Запуск ( Строка была добавлена в $startupfile. Она будет запускаться автоматически при следующем запуске antiX. \n $add_remove_text $startupfile): Графическая оболочка запуска antiX  